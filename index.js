const through = require('through2');
const licenseLs = require('license-ls');
const Buffer = require('buffer').Buffer;

module.exports = options => through.obj((vinylFile, encoding, callback) => {

  const settings = Object.assign({}, options);

  settings.production = settings.production || true;
  settings.development = settings.development || !settings.production;

  const transformedFile = vinylFile.clone();

  console.log(settings);
  
  const cb = (err, result) => {

    if (err)
      console.error(err);
    else
      transformedFile.contents = Buffer.from(result);

    callback(null, transformedFile)

  };

  licenseLs(settings)
    .then(result => cb(undefined, result))
    .catch(cb);

});