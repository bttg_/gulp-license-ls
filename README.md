# Gulp plugin for listing npm package licenses

![npm](https://img.shields.io/npm/dt/gulp-license-ls?label=Downloads)
![Maintenance](https://img.shields.io/maintenance/yes/2020?label=Maintained)
![npm](https://img.shields.io/npm/v/gulp-license-ls?label=Version)
![npm](https://img.shields.io/npm/l/gulp-license-ls?label=License)
![NodeJS](https://img.shields.io/node/v/gulp-license-ls?style=flat&label=NodeJS)
![npm](https://img.shields.io/npm/collaborators/gulp-license-ls?color=gold&label=Collaborators)

[<img src="https://media.giphy.com/media/ZDEZ9ECHhTVG5tWDVZ/giphy.gif" width="100px" />](https://www.linkedin.com/in/joskeuter/)

This plugin is a [Gulp] wrapper for [license-ls].

It can help you needly to reference all packages and licenses you use in your projects.

## Getting started

```console
npm i gulp-license-ls
```

## Example usage

Example `gulpfile.js`:

```javascript
const {
  src,
  dest
} = require('gulp');

const licenseLs = require('gulp-license-ls');

module.exports = {

  default: () =>
  src([`./package.json`])
  .pipe(licenseLs()) //or with options licenseLs({...})
  .pipe(dest('./dist/'))

};

```

For the actual options, see [license-ls].

### License

`gulp-license-ls` is [MIT] licensed.

[![static](https://img.shields.io/badge/Company-BTTG-black?style=flat)](https://www.linkedin.com/in/joskeuter/)
[![Twitter Follow](https://img.shields.io/twitter/follow/bttg_)](https://twitter.com/bttg_)
[![static](https://img.shields.io/badge/Author-Jos%20Keuter-gold?style=flat)](https://www.linkedin.com/in/joskeuter/)
[![Twitter Follow](https://img.shields.io/twitter/follow/keuter)](https://twitter.com/Keuter)

[license-ls]: https://www.npmjs.com/package/license-ls
[gulp]: https://gulpjs.com/
[mit]: https://gitlab.com/bttg_/gulp-license-ls/-/raw/master/LICENSE
